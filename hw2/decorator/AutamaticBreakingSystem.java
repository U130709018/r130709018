package decorator;

public class AutamaticBreakingSystem extends ApplicationDecorator {
	Autmobile autmobile;
	
	public AutamaticBreakingSystem(Autmobile autmobile){
		this.autmobile = autmobile;
	}
	
	public String getDescription(){
		return autmobile.getDescription() + ", AutamaticBreakingSystem";
	}
	
	public double cost(){
		return 5000 + autmobile.cost();
	}


}
