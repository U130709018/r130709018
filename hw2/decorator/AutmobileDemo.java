package decorator;

public class AutmobileDemo {

	public static void main(String[] args) {
		Autmobile autmobile = new Sivic();
		autmobile = new Airbag(autmobile);
		autmobile = new Sunroof(autmobile);
		System.out.println(autmobile.getDescription() + " " + autmobile.cost() + " $ ");
		
		Autmobile autmobile2 = new Sity();
		autmobile2 = new Airbag(autmobile2);
		autmobile2 = new MusicSystem(autmobile2);
		autmobile2 = new AutamaticBreakingSystem(autmobile2);
		autmobile2 = new Sunroof(autmobile2);
		System.out.println(autmobile2.getDescription() + " " + autmobile2.cost()+ " $ ");
		
		Autmobile autmobile3 = new Sity();
		autmobile3 = new MusicSystem(autmobile3);
		autmobile3 = new AutamaticBreakingSystem(autmobile3);
		autmobile3 = new Sunroof(autmobile3);
		System.out.println(autmobile3.getDescription() + " " + autmobile3.cost()+ " $ ");
	}

}
