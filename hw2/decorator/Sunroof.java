package decorator;

public class Sunroof extends ApplicationDecorator{
	Autmobile autmobile;
	
	public Sunroof(Autmobile autmobile){
		this.autmobile = autmobile;
	}
	
	public String getDescription(){
		return autmobile.getDescription() + ", Sunroof";
	}
	
	public double cost(){
		return 2000 + autmobile.cost();
	}

}
