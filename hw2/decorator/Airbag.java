package decorator;

public class Airbag extends ApplicationDecorator{
	Autmobile autmobile;
	
	public Airbag(Autmobile autmobile){
		this.autmobile = autmobile;
	}
	
	public String getDescription(){
		return autmobile.getDescription() + ", Airbag";
	}
	
	public double cost(){
		return 3000 + autmobile.cost();
	}

}
