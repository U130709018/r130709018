package decorator;

public class MusicSystem extends ApplicationDecorator {
	Autmobile autmobile;
	
	public MusicSystem(Autmobile autmobile){
		this.autmobile = autmobile;
	}
	
	public String getDescription(){
		return autmobile.getDescription() + ", MusicSystem";
	}
	
	public double cost(){
		return 1000 + autmobile.cost();
	}

}
