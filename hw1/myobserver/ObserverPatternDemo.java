package myobserver;

public class ObserverPatternDemo {
	   public static void main(String[] args) {
	      Subject subject = new Subject();

	      new HexaDecimalObserver(subject);
	      new DecimalObserver(subject);
	      new BinaryObserver(subject);
			
			

		}

	}
