package myobserver;

public class HexaDecimalObserver extends Observer{

	   public HexaDecimalObserver(Subject subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }
	   
	   @Override
	   public void update() {
	     System.out.println((subject.getState())); 
	   }
	}